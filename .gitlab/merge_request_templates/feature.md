# This feature adds the following

- List
- of
- features

# Related issues being closed

Fixes: #<bugnumber>

# Good citizen checklist

- [ ] I've tested this myself
- [ ] Somebody else tested this on a .iso install (e.g. VMWare)
- [ ] Somebody else tested this on a kernel/initramfs install (e.g. LibVirt)

# Docs Check

- [ ] All relevant documentation has been updated
- [ ] All relevant documentation has been spellchecked
