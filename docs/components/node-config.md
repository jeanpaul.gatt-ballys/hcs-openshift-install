# Component: Node Config

This component is responsible for configuring options for your nodes. At the moment the only configurable is the support for using CgroupsV2.

## Part Of
- Component: `node-config`
- Tags: `node-config`
- Application: `node-config`

# Index

<!-- vim-markdown-toc GFM -->

* [Node Config Variables](#node-config-variables)
    * [Node Config Configuration Example](#node-config-configuration-example)

<!-- vim-markdown-toc -->

# Node Config Variables

You can set

| Option | Required/Optional | Comment |
|--------|-------------------|---------|
| `openshift_cgroups_v2` | Optional | A boolean that determines if CGroupsV2 will be used (`True`) or not (Default, `False`) **Warning**: Not supported on OpenShift version < 4.13 |

## Node Config Configuration Example
```yaml
openshift_cgroups_v2: True
```
