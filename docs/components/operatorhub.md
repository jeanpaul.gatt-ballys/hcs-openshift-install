# Component: Operator Hub

This component is responsible for disabling Operator Hub default catalog sources when Openshift is
installed in a disconnected installation.

## Part Of
- Component: `operatorhub`
- Tags: `operatorhub`
- Application: `operatorhub`

# Index

<!-- vim-markdown-toc GFM -->

- [Component: Operator Hub](#component-operator-hub)
  - [Part Of](#part-of)
- [Index](#index)
- [Disabling Operator Hub default catalog sources](#disabling-operator-hub-default-catalog-sources)
- [Enabale sync'd catalog sources](#enabale-syncd-catalog-sources)

<!-- vim-markdown-toc -->

# Disabling Operator Hub default catalog sources

This component sets the value of `disconnected_install` variable in the `disableAllDefaultSources` parameter
of the OperatorHub resource. Default value is `false`

Operators won't install when catalog sources are not in a healthy state. Default catalog sources fail in
a disconnected installation. Therefore they need to be disabled in a disconnected installation.

# Enabale sync'd catalog sources

add the following parameters to your inventory to enable your sync'd catalogsource

catalogsources:
- name: community-operators
  repository: community-operator-index
- name: redhat-operators
  repository: redhat-operator-index

When you have sync'd operators from the marketplace or certified operator's add them to the catalogsource in your inventory.

- name: certified-operators
  repository: certified-operator-index
- name: redhat-marketplace
  repository: redhat-marketplace-index
