# Component: Group Sync

This component is responsible for installing and configuring the OpenShift Update Service Operator. It does this by performing the following tasks:

- Install the OpenShift Update Service Operator into the `openshift-update-service` namespace.
- Loop over all configured authentication providers, and create a GroupSync object if requested
- Update the cluster-version to point to the right url for the release image

## Part Of
- Component: `osus`
- Tags: `osus`
- Application: `osus`